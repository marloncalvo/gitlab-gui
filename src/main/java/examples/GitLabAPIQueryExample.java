package examples;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Issue;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.User;

import java.util.List;

public class GitLabAPIQueryExample {
    public static void getIssues(String username, String password) throws GitLabApiException {
        GitLabApi gapi = GitLabApi.oauth2Login("https://gitlab.com",username,password);
        Project project = gapi.getProjectApi().getOwnedProjects().get(2);
        List<Issue> issues = gapi.getIssuesApi().getIssues(project);
        for (Issue issue : issues) {
            System.out.println(issue.getTitle());
        }
    }
}
