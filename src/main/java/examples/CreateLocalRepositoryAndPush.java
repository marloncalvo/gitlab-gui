package examples;

import com.google.common.io.Files;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiClient;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class CreateLocalRepositoryAndPush {
    public static void main(String [] args) throws GitAPIException, IOException, GitLabApiException, URISyntaxException {
        File tempGitFolder = Files.createTempDir();

        System.out.println(tempGitFolder.getAbsolutePath());
        //GitLabApi gapi = new GitLabApi("https://gitlab.com", "xX-nMLtysDxFhYsCoChd");

        Git newGitRepo = Git.init().setDirectory(tempGitFolder).call();
        File readme = new File(newGitRepo.getRepository().getDirectory().getParent() + "/README.md");
        readme.createNewFile();

        //Project project = gapi.getProjectApi().createProject("New Git Test");*/
        //String httpUrl = project.getHttpUrlToRepo();
        String httpUrl = "https://gitlab.com/marloncalvo/new-git-test.git";

        newGitRepo
                .remoteAdd()
                .setName("origin")
                .setUri(new URIish((httpUrl))).call();

        newGitRepo
                .add()
                .addFilepattern(".").call();

        newGitRepo
                .commit()
                .setAll(true)
                .setMessage("New inital commit").call();

        newGitRepo
                .push()
                .setRemote("origin")
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider("marloncalvo", "Alphadog32"))
                .call();

        tempGitFolder.delete();
    }
}
