package examples;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Project;

import java.io.File;

public class CreateRemoteRepositoryAndClone {
    public static void main(String [] args) throws Exception {
        GitLabApi gapi = new GitLabApi("https://gitlab.com", "xX-nMLtysDxFhYsCoChd");
        Project project = gapi.getProjectApi().createProject("New Sample Project");
        String sshUrl = project.getHttpUrlToRepo();

        Git clonedNewProject = Git
                .cloneRepository()
                .setURI(sshUrl)
                .setDirectory(new File("/home/marloncalvo/Desktop/NewSampleProject"))
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider("marloncalvo", "Alphadog32"))
                .call();
/*
        Git git = Git.init().setDirectory(new File("/home/marloncalvo/Desktop/sample-repo")).call();
        String dir = git.getRepository().getDirectory().getAbsolutePath();

        File readme = new File(dir + "README.md");
        readme.createNewFile();

        git.add().addFilepattern("*");
        git.commit().setMessage("test new commit");
        git.push().setRemote("")
 */
    }
}
