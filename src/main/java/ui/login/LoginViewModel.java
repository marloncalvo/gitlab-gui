package ui.login;

import de.saxsys.mvvmfx.ViewModel;
import examples.GitLabAPIQueryExample;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginViewModel implements ViewModel {

    private StringProperty username = new SimpleStringProperty();
    private StringProperty password = new SimpleStringProperty();

    public StringProperty usernameProperty() {
        return username;
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public String getUsername() {
        return username.get();
    }

    public String getPassword() {
        return password.get();
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    protected void doLogin() {
        try {
            GitLabAPIQueryExample.getIssues(getUsername(), getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
