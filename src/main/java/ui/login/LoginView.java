package ui.login;

import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import de.saxsys.mvvmfx.ViewModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginView implements FxmlView<LoginViewModel>, Initializable {

    @FXML
    TextField UsernameField;
    @FXML
    PasswordField PasswordField;
    @FXML
    Button submitButton;

    @InjectViewModel
    private LoginViewModel viewModel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        UsernameField.textProperty().bindBidirectional(viewModel.usernameProperty());
        PasswordField.textProperty().bindBidirectional(viewModel.passwordProperty());
    }

    @FXML
    protected void loginButtonRequest(ActionEvent e) {
        viewModel.doLogin();
    }
}
